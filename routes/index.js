var express = require('express');
var router = express.Router();

var request = require('request');
var _ = require('underscore');

// GET home page
router.get('/', function(req, res) {
    res.render('index', { title: 'Rob\'s Domain Checker' });
});

// GET domain search
router.get('/search', function(req, res) {

    // data to add to URL
    var params = {
        username:       'tallrobphilp',
        password:       'Rey7hez0c7',
        outputFormat:   'json',
        domainName:     req.query.domain  
    };

    // reduce params into a query string using underscore
    var paramstring = _.reduce(params, function(memo, val, key) {
        return memo + key + '=' + val + '&';
    }, '');
    console.log(paramstring);

    // build URL and make request
    var url = 'http://www.whoisxmlapi.com/whoisserver/WhoisService?' + paramstring;
    request(url, function (error, response, body) {
        
        // parse respomnse into useful JSON
        res.contentType('json');
        var jsonbody = JSON.parse(body);

        // return error message if there is one...
        if(jsonbody.ErrorMessage) {
            res.send({error: jsonbody.ErrorMessage.msg});
            return;
        }

        // ..else return useful data
        res.send(jsonbody.WhoisRecord.registryData);
    });
});

module.exports = router;