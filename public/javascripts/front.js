(function() {
    // "Cut the mustard" test for modern JS bits and bobs
    var supports = !!document.querySelector && !!window.addEventListener;
    if ( !supports ) return;

    // click handler
    document.querySelector('input[name=search]').addEventListener('click', function(e) {
        e.preventDefault();

        // create AJAX handler
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();

        // callback to handle response
        xmlhttp.onreadystatechange = function() {

            // if we have a "DONE" state and a 200 response code
            if (xmlhttp.readyState == 4 ) {
                if(xmlhttp.status == 200) {

                    // Parse response into JSON
                    var r = JSON.parse(xmlhttp.response);

                    // If we have an error inject our useful data into DOM...
                    if(r.error) {
                        document.querySelector("#response").innerHTML = "<p>" + r.error + "</p>";
                        return;
                    }

                    // ...otherwise inject the error message
                    var html = "<p>Expiry date for " + r.domainName + " is <strong> " + r.expiresDate + "</strong></p>";
                    document.querySelector("#response").innerHTML = html;
                }
            }
        }

        // make the actual GET request to server
        var url = "/search?domain=" + document.querySelector('input[name=domain]').value;
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }, false);
})();